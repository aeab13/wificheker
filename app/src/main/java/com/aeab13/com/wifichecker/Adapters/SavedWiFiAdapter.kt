package com.aeab13.com.wifichecker.Adapters

import android.net.wifi.WifiConfiguration
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.CheckBox
import com.aeab13.com.wifichecker.R


class SavedWiFiAdapter(private val listWifiConfiguration: List<WifiConfiguration>) : RecyclerView.Adapter<SavedWiFiAdapter.SavedWifiViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedWifiViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_saved_wifi, parent, false)

        return SavedWifiViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listWifiConfiguration.count()
    }

    override fun onBindViewHolder(holder: SavedWifiViewHolder, position: Int) {
        holder.bind(listWifiConfiguration[position])
    }

    class SavedWifiViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val chkWifiNetwork: CheckBox = view.findViewById(R.id.chkWifiNetwork)

        fun bind(wifiConfiguration: WifiConfiguration) {
            chkWifiNetwork.text = wifiConfiguration.SSID.replace("\"", "")
        }
    }

}